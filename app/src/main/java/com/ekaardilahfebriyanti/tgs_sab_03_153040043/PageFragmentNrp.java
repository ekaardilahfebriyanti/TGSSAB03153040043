package com.ekaardilahfebriyanti.tgs_sab_03_153040043;

import android.app.Fragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import java.util.Calendar;

/**
 * Created by Eka Ardilah FebriY on 03/03/2018.
 */

public class PageFragmentNrp extends Fragment {
    EditText mEditTextMessage;
    Button mButtonSend;

    public static  MainActivity mainActivity;
    public static PageFragmentNrp newInstance(MainActivity activity) {
        mainActivity = activity;
        return new PageFragmentNrp();
    }
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_nrp, container, false);

        mEditTextMessage = (EditText) view.findViewById(R.id.message);
        mButtonSend = (Button) view.findViewById(R.id.sendMessage);

        mButtonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = mEditTextMessage.getText().toString().trim();
                if(!TextUtils.isEmpty(message)){
                    mEditTextMessage.setText(mainActivity.attemptSend("153040043", message));
                }
            }
        });


        return view;
    }

}
