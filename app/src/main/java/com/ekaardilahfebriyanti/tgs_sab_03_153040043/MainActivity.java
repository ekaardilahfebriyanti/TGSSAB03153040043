package com.ekaardilahfebriyanti.tgs_sab_03_153040043;

import android.app.Fragment;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;
import java.util.Date;


public class MainActivity extends AppCompatActivity {
    private WebSocketClient mWebSocketClient;
    private TextView nama,nrp;
    private TextView mEditTextChats;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nama = (TextView) findViewById(R.id.nama);
        nrp = (TextView) findViewById(R.id.nrp);
        mEditTextChats = (TextView) findViewById(R.id.tvTextChats);
        connectWebSocket();
        changeContent(PageFragmentNama.newInstance(MainActivity.this));
        nama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(PageFragmentNama.newInstance(MainActivity.this));
            }
        });
        nrp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeContent(PageFragmentNrp.newInstance(MainActivity.this));
            }
        });



    }
    public void changeContent(Fragment fragment){
        getFragmentManager().beginTransaction().replace(R.id.frameLayout, fragment).commitAllowingStateLoss();
    }
    private void connectWebSocket(){
        URI uri;
        try {
//            uri = new URI("ws://172.22.62.4:8887"); //alamat ip komputer
            uri = new URI("ws://192.168.43.203:8887"); //alamat ip laptop

        }catch (URISyntaxException e){
            e.printStackTrace();
            return;
        }
        mWebSocketClient = new WebSocketClient(uri) {
            @Override
            public void onOpen(ServerHandshake handshakedata) {
                Log.i("websocket", "Opened");
                mWebSocketClient.send("Hello from"+ Build.MANUFACTURER+" "+Build.MODEL);
            }

            @Override
            public void onMessage(String s) {
                final String message = s;
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        mEditTextChats.append(message+"\n");
                    }
                });
            }

            @Override
            public void onClose(int code, String reason, boolean remote) {
                Log.i("websocket", "Closed "+reason);
            }

            @Override
            public void onError(Exception ex) {
                Log.i("websocket", "Error "+ ex.getMessage());
            }
        };
        mWebSocketClient.connect();
    }

    public String attemptSend(String nama, String message){

        Date d = Calendar.getInstance().getTime();
        Log.i("MainActivity", ": "+nama+" "+message);
        mWebSocketClient.send (" [ "+d.toString()+ "] \n"+nama+" : "+message);
        return"" ;
    }
}
